# Image Picker Container

Take an image from gallery or camera and disply a preview

<img src="https://gitlab.com/jorgesanure-pub-dep/image-picker-container/-/raw/master/assets/preview.gif" height='300px'>

```dart
import 'package:image_picker_container/image_picker_container.dart';
...
ImagePickerContainer(
    height: 300,
    onSelect: (_, pickedImage) => print(pickedImage.path),
    initialImage: Image.network(
        'https://images.unsplash.com/photo-1523228183475-0859ff3b10b9?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MzF8fGJhbGx8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
    fit: BoxFit.cover,
)
```