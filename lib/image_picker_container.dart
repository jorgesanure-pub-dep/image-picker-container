import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerContainer extends StatefulWidget {
  final Function(Image, PickedFile) onSelect;
  final BoxFit fit;
  final Widget initialImage;
  final double width;
  final double height;

  const ImagePickerContainer(
      {Key key,
      this.onSelect,
      this.fit,
      this.initialImage,
      this.width,
      this.height})
      : super(key: key);

  @override
  _ImagePickerContainerState createState() => _ImagePickerContainerState();
}

class _ImagePickerContainerState extends State<ImagePickerContainer> {
  static final _picker = ImagePicker();

  Image _image;

  PickedFile _pickedImage;

  @override
  Widget build(BuildContext context) => InkWell(
        child: SizedBox(
          height: widget.height,
          width: widget.width,
          child: _image ?? widget.initialImage ?? SizedBox.shrink(),
        ),
        onTap: () async {
          _pickedImage = await _showPicker(context);

          if (_pickedImage != null)
            _image =
                await _loadImageFromPath(_pickedImage.path, fit: widget.fit);

          widget.onSelect?.call(_image, _pickedImage);

          setState(() {});
        },
      );

  static Future<PickedFile> _showPicker(context) => showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) => SafeArea(
            child: Container(
              color: Colors.white,
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Gallery'),
                      onTap: () async => await _getImageFromGallery().then(
                          (value) => Navigator.of(bc).pop<PickedFile>(value))),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () => _getImageFromCamera().then(
                        (value) => Navigator.of(bc).pop<PickedFile>(value)),
                  ),
                ],
              ),
            ),
          ));

  static Future<PickedFile> _getImageFromCamera() =>
      _getImage(ImageSource.camera);
  static Future<PickedFile> _getImageFromGallery() =>
      _getImage(ImageSource.gallery);

  static Future<PickedFile> _getImage(ImageSource source) => _picker
      .getImage(source: source, imageQuality: 50)
      .then((pickedFile) => pickedFile);

  static Future<Widget> _loadImageFromPath(String imagePath,
      {Widget Function(BuildContext, Object, StackTrace) errorBuilder,
      BoxFit fit}) {
    final file = File(imagePath);

    return file.exists().then((exists) {
      Image image;
      if (exists) {
        image = Image.memory(
          file.readAsBytesSync(),
          errorBuilder: errorBuilder,
          fit: fit ?? BoxFit.contain,
        );
      }
      return image;
    });
  }
}
