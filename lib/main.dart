import 'package:flutter/material.dart';
import 'package:image_picker_container/image_picker_container.dart';

void main() => runApp(MaterialApp(
      home: Scaffold(
        body: Center(
          child: ImagePickerContainer(
            height: 300,
            onSelect: (_, pickedImage) => print(pickedImage?.path),
            initialImage: Image.network(
                'https://images.unsplash.com/photo-1523228183475-0859ff3b10b9?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MzF8fGJhbGx8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    ));
